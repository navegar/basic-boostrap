# Nome do projeto  
*Descrição simples e básica do projeto*  

## Tecnologias  
* Framework tal em tal versão  
* SASS  
* Banco tal  
  
## Observações  
Detalhes específicos do projeto  
links para referências ou materiais  
Rodar no endereço `projeto.local/`  
  
## Dependências  
*Lista das dependências do projeto com instruções de instalação*  

* [Vagrant](https://www.vagrantup.com/downloads.html)  
* [Composer](https://getcomposer.org/doc/00-intro.md)  
* [Node.JS](https://nodejs.org/en/)  
* Grunt-CLI `$ npm install -g grunt-cli`   
* Bower `$ npm install -g bower`   
  
## Setup inicial do Ambiente (First Run)  
*Sequencia de passos para ambiente*  

* `$ vagrant up`  
* `$ vagrant ssh`  
* `$ cd /var/www/public`  
* `$ composer install`  
* `$ npm install`  
* `$ grunt`  
* `$ mysql`  
* `$ create database cliente;`  
* `$ exit`  
* `$ cd /var/www/public/`  
* `$ bin/cake migrations migrate`  
* `$ bin/cake migrations seed`  
* Adicionar no hosts `192.168.33.30 cliente.local` [Ajuda](http://link_para_ajuda.com.br/)  
* Rodar no endereço `cliente.local/`  
* Configurar arquivo `/src/tal.php` com as variaveis de ambiente `X`  
  
## Rotina de pull  
*Passos que devem ser executados antes do desenvolvimento (diario ou a cada pull)*  

* `$ git pull origin master`  
* `$ vagrant up`  
* `$ vagrant ssh`  
* `$ cd /var/www/public`  
* `$ composer install` *se necessário*  
* `$ npm install` *se necessário*  
* `$ grunt`
