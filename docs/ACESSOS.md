# Acessos
Senhas e acessos necessários para o desenvolvimento/homologação  
* * * Não adicionar dados de produção * * *  

## Ambiente Desenvolvimento
Desenvolvimento local, utilizar instruções do First Run no README.md  
cliente.local/  
  
## Ambiente Homologação  
url:  

### Painel (Server)
host:  
u:  
p:  

## DB
host:  
db:  
u:  
p:  

## FTP
host:  
u:  
p:  

## SSH
host:  
p:  

## CMS/Admin
host:  
u:  
p:  

## Ambiente Homologação
...
